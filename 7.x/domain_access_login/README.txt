============
Domain Login - README.txt
------------

This module depends on the Domain Access 'domain' module (7.x-3.3 at the time of writing).

This module prevents users logging into domains that aren't assigned to them.

NOTE: Once this module is enabled, a the 'bypass access check' permission can be assigned to user roles to ignore the domain login authentication provided by this module (if necessary for certain users).

Credits:

@Rory
rory <at> alcherra <dot> com